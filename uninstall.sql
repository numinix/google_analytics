#Uninstall the SQL-side of the Google Analytics plugin.
DELETE FROM `configuration` WHERE `configuration_key` LIKE '%GOOGLE_ANALYTICS%' OR `configuration_key` LIKE '%GOOGLE_CONVERSION%';
DELETE FROM `configuration` WHERE `configuration_key` = 'GADIR' LIMIT 1;
DELETE FROM `configuration_group` WHERE `configuration_group_title` LIKE '%Google Analytics%';
DROP TABLE IF EXISTS google_analytics_languages;
#ZC 1.5.0+
DELETE FROM `admin_pages` WHERE page_key = 'configGoogleAnalytics' or page_key = 'configGoogle_Analytics';    