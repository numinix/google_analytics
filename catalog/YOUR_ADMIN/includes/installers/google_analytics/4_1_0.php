<?php
    $db->Execute("DELETE FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'GOOGLE_ANALYTICS_UACCT' LIMIT 1;");
    $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET set_function = 'zen_cfg_select_option(array(\"ga4\"), ' WHERE configuration_key = 'GOOGLE_ANALYTICS_TRACKING_TYPE'");
    $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_description = '<p>Select the type of tracking you wish to use. <b>Select your tracking preference below.</b></p>' WHERE configuration_key = 'GOOGLE_ANALYTICS_TRACKING_TYPE'");