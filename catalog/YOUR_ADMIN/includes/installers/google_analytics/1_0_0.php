<?php
	$zc150 = (PROJECT_VERSION_MAJOR > 1 || (PROJECT_VERSION_MAJOR == 1 && substr(PROJECT_VERSION_MINOR, 0, 3) >= 5));
	if ($zc150 && function_exists('zen_page_key_exists') && function_exists('zen_register_admin_page'))
	{
		if (!zen_page_key_exists('configGoogleAnalytics'))
		{
			$configuration          = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'GOOGLE_ANALYTICS_VERSION' LIMIT 1;");
			$configuration_group_id = $configuration->fields['configuration_group_id'];
			if ((int) $configuration_group_id > 0)
			{
				zen_register_admin_page('configGoogleAnalytics', 'BOX_CONFIGURATION_GOOGLE_ANALYTICS', 'FILENAME_CONFIGURATION', 'gID=' . $configuration_group_id, 'configuration', 'Y', $configuration_group_id);
				$messageStack->add('Easy Google Analytics added to Configuration menu.', 'success');
			}
		}
	}

	$old = array(
		'should_enable' => (defined('GOOGLE_ANALYTICS_UACCT')) ? true : false,
		'uacct' 	=> (defined('GOOGLE_ANALYTICS_UACCT')) ? GOOGLE_ANALYTICS_UACCT : 'UA-XXXXX-Y',
		'method'	=> (defined('GOOGLE_ANALYTICS_TRACKING_TYPE')) ? GOOGLE_ANALYTICS_TRACKING_TYPE : 'universal',
		'skucode'	=> (defined('GOOGLE_ANALYTICS_SKUCODE')) ? GOOGLE_ANALYTICS_SKUCODE : 'products_id',
		'affiliation' => (defined('GOOGLE_ANALYTICS_AFFILIATION')) ? GOOGLE_ANALYTICS_AFFILIATION : '',
		'target'	=> (defined('GOOGLE_ANALYTICS_TARGET')) ? GOOGLE_ANALYTICS_TARGET : 'customers',
		'customjs'	=> array(
			'enabled'     => defined('GOOGLE_ANALYTICS_CUSTOM_AFTER') && GOOGLE_ANALYTICS_CUSTOM_AFTER == 'Enable' ? true : false,
			'javascript'	=> (defined('GOOGLE_ANALYTICS_AFTER_CODE')) ? GOOGLE_ANALYTICS_AFTER_CODE : '' 	
		),
		'conversion'=> array(
			'idnum'	=> (defined('GOOGLE_CONVERSION_IDNUM')) ? GOOGLE_CONVERSION_IDNUM : 'XXXXXXXXXX',
			'lang'	=> (defined('GOOGLE_CONVERSION_LANG')) ? GOOGLE_CONVERSION_LANG : 'en_US'
		)
	);

	
	if($old['method'] == 'Urchin'){
		$old['should_enable'] = false; // If it's urchin, they probably don't have a valid UACCT attached, meaning it's meaningless to enable the addon until the UACCT is verified.
		$old['method'] 	= 'universal';	
		if($old['customjs']['enabled'] == true){
			$old['customjs']['enabled'] = false;
		}
	}else if($old['method'] == 'Asynchronous'){
		$old['method'] = 'ga.js asynchronous';	
	}
	
	$insert_order = 1;
	
	function InsertConfiguration($title, $key, $value, $desc, $use_function = NULL, $set_function = NULL){
		global $configuration_group_id;
		global $insert_order;
		global $db;
		
		//Edit: Set function and use function cannot be null.
		if($use_function == NULL) $use_function = "''"; else $use_function = "'" . $use_function . "'";
		if($set_function == NULL) $set_function = "''"; else $set_function = "'" . $set_function . "'";
			
		$db->Execute("INSERT INTO " . TABLE_CONFIGURATION . "  (`configuration_id`, `configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES (NULL, '" . $title . "', '" . $key . "', '" . $value . "', '" . $desc . "', $configuration_group_id, $insert_order, NOW(), NOW(), $use_function, $set_function);");
	
		$insert_order++;	
	}
	
	$value = ($old['should_enable'] == true) ? 'Enabled' : 'Disabled';	
	InsertConfiguration('Analytics Enabled', 'GOOGLE_ANALYTICS_ENABLED', $value, 'Enables / disables this plugin.', NULL, 'zen_cfg_select_option(array("Enabled", "Disabled"), ');
	InsertConfiguration('Analytics Account', 'GOOGLE_ANALYTICS_UACCT', $old['uacct'], 'This number is the unique ID you were given by Google when you registered for your Google Analytics account. <b>Enter your Google Analytics account number below. (It starts with "UA-")</b>');
	
	InsertConfiguration('Target Address', 'GOOGLE_ANALYTICS_TARGET', $old['target'], 'This element is used in conjunction with Google E-Commerce Tracking. It indicates how you want your "transactions" to be identified in your Analytics reports.<br /><br />Addresses consist of City, State and Country.<br /><br />This information can help you to determine locality of orders places, shipped to, or billed to. <b>Which address type do you want to use for recording transaction information?</b><br /><br />', NULL, 'zen_cfg_select_option(array("customers", "delivery", "billing"), ');
	
	InsertConfiguration('Affiliation', 'GOOGLE_ANALYTICS_AFFILIATION', $old['affiliation'], 'This <em>optional</em> tracking element is used in conjunction with Google E-Commerce Tracking.<br /><br />The affiliation tag describes the affiliating store or processing site.<br /><br />It can be used if you have multiple stores (or web sites) in various locations and is used to track from which location a particular sale originated. <b>If you have one, enter your optional partner or store affiliation in the space provided below.</b><br />');
	
	InsertConfiguration('Use SKU code', 'GOOGLE_ANALYTICS_SKU_CODE', $old['skucode'], 'This tracking element is used in conjunction with Google Analytics E-Commerce tracking.<br /><br />It enables you to track which products perform better than others using the Product ID, or the Product Model as a unique identifier. <b>Indicate which identifier you want to use to track product performance by selecting one of the options below.</b>', NULL, 'zen_cfg_select_option(array("products_id", "products_model"),');
	
  	InsertConfiguration('AdWords Conversion Tracking Active', 'GOOGLE_CONVERSION_ACTIVE', 'No', 'Did you want to activate the conversion tracking?', NULL, 'zen_cfg_select_option(array("Yes", "No"),');
        
	InsertConfiguration('AdWords Conversion Tracking Number', 'GOOGLE_CONVERSION_IDNUM', $old['conversion']['idnum'], 'If you activated Conversion Tracking in the previous section, then you <b>must</b> enter your unique Google Conversion Tracking ID in place of the "XXXXXXXXXXX" shown in the space provided below.<br /><br />If you have activated Conversion Tracking and you do not enter your number below, tracking will not work. <b>Enter your AdWords Conversion Tracking ID Number below.</b>');
	
	InsertConfiguration('Google AdWords Language', 'GOOGLE_CONVERSION_LANG', $old['conversion']['lang'], 'Select the language to be used. The default is "English US". <b>Select your language below</b><br />', NULL, 'zen_cfg_pull_down_google_languages(');
	
	InsertConfiguration('Analytics Tracking Type', 'GOOGLE_ANALYTICS_TRACKING_TYPE', $old['method'], 'Select the type of tracking you wish to use. The default is the "universal" type. You have the ability to change this to the older "ga.js" method. <b>Select your tracking preference below.</b><br />', NULL, 'zen_cfg_select_option(array("universal", "ga.js", "ga.js asynchronous"), ');
	
	$value = ($old['customjs']['enabled'] == true) ? 'Enable' : 'Disable';
	InsertConfiguration('Custom Tracking JS Enabled', 'GOOGLE_ANALYTICS_CUSTOM_CODE_ENABLED', $value, 'Enables / disables inclusion of custom Javascript.', NULL, 'zen_cfg_select_option(array("Enable", "Disable"), ');
	
	InsertConfiguration('Custom Tracking JS', 'GOOGLE_ANALYTICS_CUSTOM_CODE', addslashes($old['customjs']['javascript']), 'If you wish to include any Javascript code after the main tracking segment, insert it here.<br /><br />', NULL, 'zen_cfg_textarea(');
	
	// Step 5: Place the languages
	
	$db->Execute("DROP TABLE IF EXISTS " . TABLE_GOOGLE_ANALYTICS_LANGUAGES . ";");
	$db->Execute("CREATE TABLE " . TABLE_GOOGLE_ANALYTICS_LANGUAGES . " (
	  languages_id int(11) NOT NULL auto_increment,
	  name varchar(50) NOT NULL default '',
	  code char(10) NOT NULL default '',
	  sort_order int(3) default NULL,
	  PRIMARY KEY  (languages_id),
	  KEY idx_languages_name_zen (name)
	);");
	
	$db->Execute("INSERT INTO " . TABLE_GOOGLE_ANALYTICS_LANGUAGES . " VALUES 
	(NULL,'Chinese (simplified) - ???','zh_CN',1),
	(NULL,'Chinese (traditional) - ???','zh_TW',2),
	(NULL,'Danish - Dansk','da',3),
	(NULL,'Dutch - Nederlands','nl',4),
	(NULL,'English (Australia)','en_AU',5),
	(NULL,'English (UK))','en_GB',6),
	(NULL,'English (US)','en_US',7),
	(NULL,'Finnish - suomi','fi',8),
	(NULL,'French - Français','fr',9),
	(NULL,'German - Deutsch','de',10),
	(NULL,'Hebrew - ???','iw',11),
	(NULL,'Italian - Italiano','it',12),
	(NULL,'Japanese - ???','ja',13),
	(NULL,'Korean - ???','ko',14),
	(NULL,'Norwegian - Norsk','no',15),
	(NULL,'Polish - polski','pl',16),
	(NULL,'Portuguese (Brazil) - Português (Brasil)','pt_BR',17),
	(NULL,'Portuguese (Portugal) - Português (Portugal)','pt_PT',18),
	(NULL,'Russian - ???????','ru',19),
	(NULL,'Spanish - Español','es',20),
	(NULL,'Swedish - Svenska','sv',21),
	(NULL,'Turkish - Türkçe','tr',22);");