<?php
$db->Execute("INSERT IGNORE INTO " . TABLE_CONFIGURATION . " 
	(`configuration_title`, `configuration_key`, `configuration_value`, `configuration_description`, `configuration_group_id`, `sort_order`, `last_modified`, `date_added`, `use_function`, `set_function`) VALUES 
	('Google Conversion Label', 'GOOGLE_CONVERSION_LABEL', 'purchase', 'Enter your Google Conversion Label (can be generated in Google Adwords or you can create a custom label for tracking elsewhere)', ".(int)$configuration_group_id.", 12, NULL, now(), NULL, NULL)
");