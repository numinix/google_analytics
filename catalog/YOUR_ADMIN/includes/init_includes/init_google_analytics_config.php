<?php
if (!defined('IS_ADMIN_FLAG'))
{
    die('Illegal Access');
}

$module_constant = 'GOOGLE_ANALYTICS_VERSION';
$module_installer_directory = DIR_FS_ADMIN . 'includes/installers/google_analytics';
$module_name = "Google Analytics";
$zencart_com_plugin_id = 1827; // from zencart.com plugins - Leave Zero not to check
//Just change the stuff above... Nothing down here should need to change

$configuration_group_id = '';
if (defined($module_constant)) {
    $current_version = constant($module_constant);
} else {
    // Step 1: Purge any old Simple Analytics Code (for versions older than 1.2.X)
    $zc150 = (PROJECT_VERSION_MAJOR > 1 || (PROJECT_VERSION_MAJOR == 1 && substr(PROJECT_VERSION_MINOR, 0, 3) >= 5));
    $sql = array(
        "DELETE FROM " . TABLE_CONFIGURATION . " WHERE `configuration_key` LIKE '%GOOGLE_ANALYTICS%' OR `configuration_key` LIKE '%GOOGLE_CONVERSION%';",
        "DELETE FROM " . TABLE_CONFIGURATION_GROUP . " WHERE `configuration_group_title` LIKE '%Google Analytics%';"
    );
    if ($zc150) $sql[] = "DELETE FROM " . TABLE_ADMIN_PAGES . " WHERE page_key = 'configGoogleAnalytics' or page_key = 'configGoogle_Analytics';"; 

    foreach($sql as $query){
        $db->Execute($query);
    }

    $configuration = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION_GROUP . " WHERE configuration_group_title = 'Google Analytics' OR configuration_group_title = 'Simple Google Analytics' OR configuration_group_title = 'Easy Google Analytics';");
    if ($configuration->RecordCount() > 0) {
        while (!$configuration->EOF) {
        $db->Execute("DELETE FROM " . TABLE_CONFIGURATION . " WHERE configuration_group_id = " . $configuration->fields['configuration_group_id'] . ";");
        $db->Execute("DELETE FROM " . TABLE_CONFIGURATION_GROUP . " WHERE configuration_group_id = " . $configuration->fields['configuration_group_id'] . ";");
        $configuration->MoveNext();
        }
    }
    $db->Execute("DELETE FROM " . TABLE_CONFIGURATION . " WHERE configuration_group_id = 0;");
    $db->Execute("DELETE FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = '';");
    
    $current_version = "0.0.0";
    $db->Execute("INSERT INTO " . TABLE_CONFIGURATION_GROUP . " (configuration_group_title, configuration_group_description, sort_order, visible) VALUES ('" . $module_name . "', 'Set " . $module_name . " Options', '1', '1');");
    $configuration_group_id = $db->Insert_ID();

    $db->Execute("UPDATE " . TABLE_CONFIGURATION_GROUP . " SET sort_order = " . $configuration_group_id . " WHERE configuration_group_id = " . $configuration_group_id . ";");

    $db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES
                    ('Version', '" . $module_constant . "', '0.0.0', 'Version installed:', " . $configuration_group_id . ", 0, NOW(), NOW(), NULL, NULL);");
}
if ($configuration_group_id == '') {
    $config = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION . " WHERE configuration_key= '" . $module_constant . "'");
    $configuration_group_id = $config->fields['configuration_group_id'];
}

// Delete obsolete new_install.php
if (file_exists(DIR_FS_ADMIN . 'includes/installers/google_analytics/' . 'new_install.php')) {
    @unlink(DIR_FS_ADMIN . 'includes/installers/google_analytics/' . 'new_install.php');
}

$installers = scandir($module_installer_directory, 1);

$newest_version = $installers[0];
$newest_version = substr($newest_version, 0, -4);

sort($installers);
if (version_compare($newest_version, $current_version) > 0) {
    foreach ($installers as $installer) {
        if (version_compare($newest_version, substr($installer, 0, -4)) >= 0 && version_compare($current_version, substr($installer, 0, -4)) < 0) {
            include($module_installer_directory . '/' . $installer);
            $current_version = str_replace("_", ".", substr($installer, 0, -4));
            $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '" . $current_version . "' WHERE configuration_key = '" . $module_constant . "' LIMIT 1;");
            $messageStack->add("Installed " . $module_name . " v" . $current_version, 'success');
        }
    }
}

if ($newest_version == 'upgrade_1.X') {
    
    if (file_exists(DIR_FS_ADMIN . 'includes/installers/google_analytics/' . 'upgrade_1.X.php')) {
        @unlink(DIR_FS_ADMIN . 'includes/installers/google_analytics/' . 'upgrade_1.X.php');
    }

    $installers = scandir($module_installer_directory, 1);

    $newest_version = $installers[0];
    $newest_version = substr($newest_version, 0, -4);

    sort($installers);
    if (version_compare($newest_version, $current_version) > 0) {
        foreach ($installers as $installer) {
            if (version_compare($newest_version, substr($installer, 0, -4)) >= 0 && version_compare($current_version, substr($installer, 0, -4)) < 0) {
                include($module_installer_directory . '/' . $installer);
                $current_version = str_replace("_", ".", substr($installer, 0, -4));
                $db->Execute("UPDATE " . TABLE_CONFIGURATION . " SET configuration_value = '" . $current_version . "' WHERE configuration_key = '" . $module_constant . "' LIMIT 1;");
                $messageStack->add("Installed " . $module_name . " v" . $current_version, 'success');
            }
        }
    }
}



if (!function_exists('plugin_version_check_for_updates')) {

    function plugin_version_check_for_updates($fileid = 0, $version_string_to_check = '') {
        if ($fileid == 0) {
            return FALSE;
        }
        $new_version_available = FALSE;
        $lookup_index = 0;
        $url = 'http://www.zen-cart.com/downloads.php?do=versioncheck' . '&id=' . (int) $fileid;
        $data = json_decode(file_get_contents($url), true);
        // compare versions
        if (version_compare($data[$lookup_index]['latest_plugin_version'], $version_string_to_check) > 0) {
            $new_version_available = TRUE;
        }
        // check whether present ZC version is compatible with the latest available plugin version
        if (!in_array('v' . PROJECT_VERSION_MAJOR . '.' . PROJECT_VERSION_MINOR, $data[$lookup_index]['zcversions'])) {
            $new_version_available = FALSE;
        }
        if ($version_string_to_check == true) {
            return $data[$lookup_index];
        } else {
            return FALSE;
        }
    }

}

// Version Checking
if ($zencart_com_plugin_id != 0) {
    if ($_GET['gID'] == $configuration_group_id) {
        $new_version_details = plugin_version_check_for_updates($zencart_com_plugin_id, $current_version);
        if ($new_version_details != FALSE) {
            $messageStack->add("Version " . $new_version_details['latest_plugin_version'] . " of " . $new_version_details['title'] . ' is available at <a href="' . $new_version_details['link'] . '" target="_blank">[Details]</a>', 'caution');
        }
    }
}