<?php
/* Begin Easy Google Analytics */
if(defined('GOOGLE_ANALYTICS_ENABLED') && GOOGLE_ANALYTICS_ENABLED == "Enabled"){
    switch(GOOGLE_ANALYTICS_TRACKING_TYPE) {
        case 'ga4':
        default:
            // will not run GA4 script again for these pages
            $pagelistGA4 = array(FILENAME_PRODUCT_INFO, FILENAME_CHECKOUT_SHIPPING, FILENAME_CHECKOUT_SUCCESS, FILENAME_ONE_PAGE_CHECKOUT);
            if (!in_array($_GET['main_page'], $pagelistGA4)) {
?>
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo GOOGLE_ANALYTICS_MEASUREMENT_ID ?>"></script>
            <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
                <?php
                if (GOOGLE_ANALYTICS_DEBUG_MODE == "Enabled") {
                    ?>
                    gtag('config', '<?php echo GOOGLE_ANALYTICS_MEASUREMENT_ID ?>', { 'debug_mode': true });
                    <?php
                } else {
                    ?>
                    gtag('config', '<?php echo GOOGLE_ANALYTICS_MEASUREMENT_ID ?>');
                    <?php
                }
                ?>
                </script>
<?php
            }

            if ($_GET['main_page'] == FILENAME_CHECKOUT_SUCCESS) {
?>
                <script defer src="https://www.googletagmanager.com/gtag/js?id=<?php echo GOOGLE_ANALYTICS_MEASUREMENT_ID ?>"></script>
                <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
            
                <?php
                if (defined('GOOGLE_ANALYTICS_DEBUG_MODE') && GOOGLE_ANALYTICS_DEBUG_MODE == "Enabled") {
                    ?>
                    gtag('config', '<?php echo GOOGLE_ANALYTICS_MEASUREMENT_ID ?>', { 'debug_mode': true });
                    <?php
                } else {
                    ?>
                    gtag('config', '<?php echo GOOGLE_ANALYTICS_MEASUREMENT_ID ?>');
                    <?php
                }
?>
                </script>
<?php
            }
        break;
    }
    if ($_GET['main_page'] != FILENAME_CHECKOUT_SUCCESS) {

    } else {
        ?>
        <script>
        <?php
        $order_query = "select orders_id, " . GOOGLE_ANALYTICS_TARGET . "_city as city, " . GOOGLE_ANALYTICS_TARGET . "_state as state, " . GOOGLE_ANALYTICS_TARGET . "_country as country from " . TABLE_ORDERS . " where customers_id = :customersID order by date_purchased desc limit 1";
        $order_query = $db->bindVars($order_query, ':customersID', $_SESSION['customer_id'], 'integer');
        $ga_order = $db->Execute($order_query);

        $google_analytics = array();

        $totals = $db->Execute("select value, class from " . TABLE_ORDERS_TOTAL . " where orders_id = '" . (int)$ga_order->fields['orders_id'] . "' and (class = 'ot_total' or class = 'ot_tax' or class = 'ot_shipping')");

        while(!$totals->EOF) {
            $google_analytics[$totals->fields['class']] = number_format($totals->fields['value'], 2, '.', '');
            $totals->MoveNext();
        }

	    $products = $db->Execute("select products_id, " . (defined('GOOGLE_ANALYTICS_SKU_CODE') ?  GOOGLE_ANALYTICS_SKU_CODE : 'products_id') . " as skucode, products_name, final_price, products_quantity from " . TABLE_ORDERS_PRODUCTS . " where orders_id = '" . $ga_order->fields['orders_id'] . "'");
	    $items = "";
        $ga4_items = [];
	    while(!$products->EOF) {
		    $category_query = "select cd.categories_name from " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c left join " . TABLE_CATEGORIES_DESCRIPTION . " cd on (cd.categories_id = p2c.categories_id) where p2c.products_id = '" . $products->fields['products_id'] . "' and cd.language_id = :languagesID limit 1";
		    $category_query = $db->bindVars($category_query, ':languagesID', $_SESSION['languages_id'], 'integer');
		    $category = $db->Execute($category_query);
		    switch(GOOGLE_ANALYTICS_TRACKING_TYPE) {
                case 'ga4':
                default:
                    $new_data = [
                        'item_id' => addslashes($products->fields['skucode']),
                        'item_name' => addslashes($products->fields['products_name']),
                        'item_category' => addslashes($category->fields['categories_name']),
                        'price' => (float) number_format($products->fields['final_price'], 2, '.', ''),
                        'quantity' => (int) $products->fields['products_quantity'],
                        'currency' => "USD"
                    ];
                    
                    $ga4_items[] = $new_data;
                    
                break;
		    }
		    $products->MoveNext();
	    }
	    switch(GOOGLE_ANALYTICS_TRACKING_TYPE) {
            case 'ga4':
            default:
                $shipping = isset($google_analytics['ot_shipping']) ? $google_analytics['ot_shipping'] : 0;
?>
        gtag("event", "purchase", {
            transaction_id: "<?php echo $ga_order->fields['orders_id']; ?>",
            affiliation: "<?php echo GOOGLE_ANALYTICS_AFFILIATION; ?>",
            value: <?php echo (float) $google_analytics['ot_total']; ?>,
            tax: <?php echo (float) $google_analytics['ot_tax']; ?>,
            shipping: <?php echo $shipping; ?>,
            currency: "USD",
            items: <?php echo json_encode($ga4_items); ?>
        });
<?php           
            break;
	    }
?>
	//--></script>
<?php
    }
}
?>