This plugin supports ZenCart 1.3.8 and above.

Please consult readme.html inside the documentation directory for further instructions.

For reference, our updated version of the module is "Easy Google Analytics". The old version is "Simple Google Analytics".
